#!/usr/bin/env bash
if ! [ -x "$(command -v gnuplot)" ]; then #Si no exite gnuplot...
  echo >&2 "Instalando GNUPLOT para dibujar las graficas"
  sudo apt-get install gnuplot-x11 #Instalamos gnuplot
  echo >&2 "Ejecuta de nuevo el script para realizar el benchsuite"
  exit #Salimos del programa
fi
file=resultado.log #Nombre del archivo .log
data=data.dat #Nombre del arhivo .dat
nexec=$1 #Numero de iteraciones
rm resultado.log 2> /dev/null #Comentar si no quereis que se borre el fichero resultado cada vez que ejecuteis el script
touch $file #Creamos el archivo .log
lscpu >> $file #Introducimos los datos del ordenador
lshw >> $file
make build > /dev/null #Compilamos la solución
./build/mandelbrot mandelbrot $nexec
./build/mandelbrotParalelo mandelbrotParalelo $nexec
./build/mandelbrotParaleloA2b mandelbrotParaleloA2b $nexec
./build/mandelbrotParaleloA2d mandelbrotParaleloA2d $nexec
array=$(awk '{ print $3 }' data.dat)
set -- $array 
speedup1=$(echo "scale=4;$2/$1" | bc -l)
speedup2=$(echo "scale=4;$3/$1" | bc -l)
speedup3=$(echo "scale=4;$4/$1" | bc -l)
echo "4 speedUpParaleloOMP $speedup1" >> $data 
echo "5 speedUpParaleloA2b $speedup2" >> $data
echo "6 speedUpParaleloA2d $speedup3" >> $data 
diff -s mandelbrot mandelbrotParalelo
diff -s mandelbrotParaleloA2b mandelbrot
diff -s mandelbrotParaleloA2d mandelbrot
gnuplot 2> /dev/null <<EOF
set term png size 1000,1000
set output "graficaPractica4.png"
set style line 1 lc rgb 'red'
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'yellow'
set style line 4 lc rgb 'black'
set style line 5 lc rgb 'grey'
set style line 6 lc rgb 'brown'
set style line 7 lc rgb 'green'
set title "Grafica P4 (mandelbrot)"
unset key
set style fill solid
set yrange [0:70]
set xrange [-2:8]
set boxwidth 0.5 abs
set ylabel 'Tiempo' offset 2,1 rotate by 0
plot 'data.dat' every ::0::0 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 1,'' using 1:3:3 with labels,'data.dat' every ::1::1 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 2,'' using 1:3:3 with labels,'data.dat' every ::2::2 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 3,'' using 1:3:3 with labels, 'data.dat' every ::3::3 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 4,'' using 1:3:3 with labels, 'data.dat' every ::4::4 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 4,'' using 1:3:3 with labels, 'data.dat' every ::5::5 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 4,'' using 1:3:3 with labels, 'data.dat' every ::6::6 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 4,'' using 1:3:3 with labels
EOF
rm data.dat > /dev/null #Borramos el archivo data.dat
rm mandelbrot > /dev/null
rm mandelbrotParalelo > /dev/null
rm mandelbrotParaleloA2b
rm mandelbrotParaleloA2d 
echo -ne "\033[0K\r"
echo "GRAFICA CREADA"