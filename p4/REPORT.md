# P4: Mandelbrot

## Índice

1. Explicación del código
2. Comentario sobre los resultados
3. Mejoras del código (*Errores*)
4. Compilación y ejecución del código
5. EJERCICIOS

# 1. Explicación del código

## ***Explicación de la paralelización del codigo***
Básicamente lo que hacemos es paralelizar con #pragma omp for los tres bucles principales
del programa, de esta manera trabajan todos los thread, y no hay desorden ya que cada
región paralela de los for tiene una barrera implícita que hace que todos los
thread esperen a que acabe el último. 
La única parte que no es paralela es cuando usamos la directiva single para asignar los
valores a las variables r, g y b (explicado con más detalle en la sección de errores).
Sobre el ejercicio 2 he escogido la opción A: en la cual he creado dos archivos nuevos llamados
mandelbrotParaleloA2c y mandelbrotParaleloA2d, donde contengo los apartados c y d
del enunciado (función runtime y variables privadas a cada thread). En el primero programo
los for como runtime y en el segundo ceo un array donde voy metiendo todos los valores
para luego recorrerlo y quedarme con el mayor. 

## ***Explicación del programa principal***
De nuevo, el programa principal se ejecuta varias veces dependiendo del número de iteraciones
que nosotros le pasemos, solo ejecuta varias veces la región paralela, que calculará un tiempo
media de ejecución para después guardarlo en un archivo 'data.dat'. Hacemos lo mismo con el
programa secuencial para después poder calcular el speedup con los dos tiempos.



## ***Explicación del archivo 'benchsuite.sh'***
En el benchsuite ejecutamos los dos programas (secuencial y paralelo) y seguidamente, hacemos la
división con ambos tiempos para calcular el speedup. Utilizamos la instrucción diff para comprobar
que la imagen es igual y por último pintamos los 3 valores en una gráfica para sacar conclusiones
de los datos obtenidos

# 2. Comentario sobre los resultados

## ***Observación de resultados***
Observamos un speedup bueno, que en líneas genéralos no alcanza el 0,5. Podemos ver que el programa
paralelo genera la misma imagen gracias al 'diff' que ejecutamos en el benchsuite, y además
lo hace en la mitad del tiempo que el secuencial

# 4. Compilación y ejecución del código

## ***Compilación y ejecución***

Para compilar y ejecutar el código debemos de hacer lo siguiente:
	- colocarnos en 'p4'
	- ejecutar './benchsuite'
	- los parámetros que le pasamos en este caso es el número de veces que queremos que se ejecute 
	nuestros programas

**Ejemplo:**
```sh
user@x:/ppctr-paralela/p4$ ./benchsuite.sh 100
```

# 5. EJERCICIOS

## EJERCICIO 1

1. ***Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que 
has utilizado (sólo las que no hayas explicado en prácticas anteriores).***
Hay una nueva directiva usada en la práctica: 
	- critical: esto lo que hace es generar una región crítica, la cual realizan todos los threads
	pero solo accede un thread a él en cada instante de tiempo. 

2. ***Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.***
Las variables que pongo como privadas son todos los contadores o índices que se utilizan
en los bucles, estos son: i, j, y, x y c. Las demás se supone se quedan como compartidas
ya que necesitamos modificarlas en la región paralela y seguir mas adelante con ellas 
modificadas.

3. ***Explica brevemente cómo se realiza el paralelismo en este programa, indicando las
partes que son secuenciales y las paralelas.***
El primer bucle calcula para todos los píxels el rango de x e y, esto no afecta el orden en el 
que se haga por lo tanto podemos paralelizar con una directiva for todo el bucle para que lo haga
de una forma más rápida. A continuación, determinamos la coloración de cada píxel, de nuevo, ocurre lo 
mismo donde no se importa en el orden en el que se haga, sino que se ejecute para todos los píxeles.
Esta parte tarda un poco más que la anterior ya que contiene una región crítica a la que accederá
solo un thread en cada instante de tiempo. Debemos reservar espacio para los valores r, g y b, esta 
parte necesitamos que la haga un solo thread, por eso utilizamos la directiva 'single'. Por último 
damos color a cada píxel, de nuevo, no afecta el orden en el que los threads vayan ejecutando su
trabajo. 

## EJERCICIO 2
***Opción A: Región crítica***
*El segundo bloque de código etiquetado con el comentario /*Determine the coloring of
each pixel*/, obtiene el valor de la variable c_max, que debe ser compartida. Esto genera
la necesidad de proteger el acceso a esta variable mediante una sección crítica. Realizar
cuatro implementaciones distintas de la sección crítica y comprueba el rendimiento de
cada una de ellas, calculando el speedup obtenido sólo por ese bucle. Explica los resultados 
obtenidos apoyándote en gráficas comparativas. Las cuatro implementaciones son
las siguientes:
	a. Implementación mediante directivas OpenMP.
	b. Implementación mediante funciones de runtime.
	c. Implementación secuencial de esa parte del código.
	d. Implementación con variables privadas a cada thread y selección del máximo de
	todas ellas.*

El programa paralelo con funciones de Open MP, contiene el speedup más alto, aun así
no deja de ser un buen tiempo ya que realiza el mandelbrot a la mitad de tiempo que el programa
secuencial. En el caso de los otros dos (runtime y variables privadas a cada thread) vemos un 
speedup aún mejor, ambos con un valor similar (rondando un speedup de 0.43). Esto se debe a 
que no maneja una zona critica como tal donde solo entra un thread en cada instante de tiempo.