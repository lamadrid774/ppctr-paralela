# P2: MatMul

Autor: Álvaro Lamadrid Agudo

## Índice

1. Explicación del código
2. Comentario sobre los resultados
3. Mejoras del código (*Errores*)
4. Compilación y ejecución del código
5. EJERCICIOS

# 1. Explicación del código

## Explicación de la paralelización del código
El archivo 'matmulParalelo.c' realiza todas las operaciones con las matrices: secuencial, paralelo normal,
paralelo con static dynamic y guided. De esta manera en el mismo archivo podemos calcular todos los speedup
para mostrarlos y comprobar resultados. 
Paralelización del método matmul: simplemente lo que he hecho es abrir una región paralela poniendo como 
variables privada i,j y k (utilizadas en los for), esto hará que en esta sección se creen varios hilos
para ejecutar aquello que hay dentro de la región paralela. A continuación, en cada for utilizo la 
operación de Open MP '#pragma omp for' para los for principales, ya que la multiplicación se hace por 
cada columna y fila sobre un determinado número, sin que esto afecte al resto de números, por lo que 
nos da igual en que orden finalicen los thread que no va a afectar nuestras operaciones. 
Paralelización con static: hacemos la misma operación que en el apartado anterior solo que programando
los for como static, esto lo que hará será dividir el trabajo por igual entre todos los threads.
Paralelización con dynamic: en este apartado al estar programados los for como dinámicos, a los hilos se les 
asigna el trabajo cuando van acabando.
Paralelización con guided: hacemos lo mismo, pero programando los for como guided; esto hace que a cada hilo
se le vaya asignando un trabajo cada vez menor.

## Explicacion del programa principal
En la main ejecuto varias veces la parte paralela. El número de veces que se ejecutan depende del número de
iteraciones que se le pasen. Por lo tanto, lo que hace es ir calculando los speedup de cada tipo de programa 
(paralelo, paralelo con static, con guided, etc.) y va calculando la media para, al final, escribir esta 
media de los speedup en un arvhivo llamado 'f' que contendrá los resultados. 

## Explicación del archivo 'benchsuite.sh' 
En el benchsuit el primer paso es comprobar si tenemos instalado el gnuplot para generar la gráfica resultante, 
en caso de no tenerlo instalado, este se instalará con permiso del usuario. A continuación, creamos el archivo
'file', que será un archivo donde guardaremos los datos del ordenador y lo que deba mostrar el programa ejecutado,
en este caso, los tiempos generados. El archivo 'data.dat' que es de donde se van a leer los datos y un parámetro
$nexec, el cual nos dejará definir cuantas iteraciones deberá ejecutar nuestro programa principal. Tras introducir 
los datos del ordenador y del programa, procedemos a compilar nuestro porgrama y, posteriormente, ejecutarlo. 
Una vez ejecutado y compilado se creará la gráfica partiendo de los valores escritos en nuestro archivo 'data.dat'

# 2. Comentario sobre los resultados

## Observación de resultados 
Tras haber hecho el código, compilarlo, ejecutarlo una serie de veces y generar una serie de resultados
es el momento de comprobar los resultados obtenidos y formular conclusiones. Observamos la gráfica y lo que tenemos son
los 4 speedup medios que hemos conseguido con el programa. Tras haber realizado la ejecución varias veces, por 
lo general salen siempre los mismo speedup (lógico) y puedo sacar las siguientes conclusiones:
	- El speedup del programa paralelo no es muy bueno, por lo general saca valores de más de 0,9. Eso quiere
	decir que nuestro porgrama paralelo no es mucho más rápido que el secuencial.
	- Sin embargo, observamos una gran diferencia al ver el speedup del programa paralelo con static, suele estar sobre el
	0.12 y 0.16. Es un speedup bastante bueno comparado con el programa paralelo, esot quiere decir que tarda mucho menos 
	tiempo que el programa paralelo normal.
	- Pero nuestro método paralelo más rápido es el dynamic. Si seguimos la teoría podemos comprobar
	que esto es correcto, ya que (como hemos explicado anteriormente) lo que hace es asignarle trabajo a cada hilo que ha
	terminado.
	- Por último tenemos el speedup del guided, este por lo general suele tener un speedup muy similar al static, a veces 
	mayor y a veces menor, por lo que podemos decir que es peor que el dynamic e igual que el static. 

# 3. Mejoras del código (*Errores*)

## Errores y correcciones producidas en el código
Obviando errores de compilación y de ejecución, quiero comentar un 'error' o mejor dicho una mejora sobre el código
al compararlo con un alumno de clase. Un compañero tenía (en cada método paralelo) puestas las variables privadas
(i j y k) pues cada una de ellas en los respectivos for donde se usaban, mientras que en mi código estaban puestas
como privada en el inicio de la región paralela (en la instrucción #pragma omp parallel). Por lo que decidimos comparar
los speedup para ver que método funcionaba mejor, tras hacer varias ejecuciones y comprobarlo vimos que mi compañero
(Francisco Álamo García) tenía un mejor speedup que el mío, por lo que decidí cambiar mi programa. Como he dicho, no 
es un error, sino una mejora. * (Además creo que lo comentamos en clase contigo).

# 4. Compilación y ejecución del código

## ***Compilacion y ejecucion***

Para compilar y ejecutar el código debemos de hacer lo siguiente:
	- colocarnos en 'p2'
	- ejecutar './benchsuite'
	- los parámetros que le pasamos primero es el tamaño que queremos que tenga la matriz, seguido del tamaño del
	bloque y por último, el numero de veces que queremos que se ejecute nuestro programa

**Ejemplo:**
```sh
user@x:/ppctr-paralela/p4$ ./benchsuite.sh 1000 [100] 50
```

# 5. EJERCICIOS

## EJERCICIO 1
1. Explica brevemente la función que desarrolla cada una de las directivas y funciones
de OpenMP que has utilizado.
	- #pragma omp parallel: inicio de la región paralela donde al llegar un hilo genera
	más hilos para trabajar de forma paralela
	- #pragma omp for: define que las iteraciones que realiza el bucle indicado con esta instruccion
	se realicen de forma paralela con los threads creados previamente por parallel.

2. Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa
etiqueta.
Las variables que he etiquetado son las constantes de cada método: la i, la j y la k, las cuales pongo
como privadas para que se usen y editen solo en dicha región paralela.

3. Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.
El trabajo se reparte de una manera en la cual todos los threads tienen una misma cantidad
de trabajo que tienen que ejecutar. Ya que se divide todo el trabajo entre el número de threads
que haya.

4. Calcula la ganancia (speedup) obtenido con la paralelización.
El speedup del programa paralelo no es muy bueno, por lo general saca valores de más de 0,9. Eso quiere
decir que nuestro programa paralelo no es mucho más rápido que el secuencial.

## EJERCICIO 2

1. *¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida
sin la cláusula schedule? Explica con detalle a qué se debe esta diferencia.*

La diferencia es bastante grande, hemos pasado de tener un speedup de 0,9 a no llegar
a un speedup de 0,2 en ninguno de los tres casos.


2. *¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica
por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las
operaciones que tiene que realizar cada thread.*

El que mejor resultado obtiene es la programación como dynamic, ya que los threads van ejecutándose
y se les va asignando trabajo cuando acaban.


3. *Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función
de cómo se reparte la carga de trabajo.*

El tamaño de bloque óptimo debería ser toda la carga de trabajo dividido entre los núcleos
del procesador.


4. *Para el algoritmo dynamic determina experimentalmente el tamaño del bloque
óptimo. Para ello mide el tiempo de respuesta y speedup obtenidos para al menos
10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y
explicar detalladamente estos resultados.*

Tamaño 1: speedup: 0.121
Tamaño 5: speedup: 0.130
Tamaño 10: speedup: 0.128
Tamaño 50: sppedup: 0.113
Tamaño 100: sppedup: 0.124
Tamaño 150: sppedup: 0.145
Tamaño 200: sppedup: 0.117
Tamaño 500: sppedup: 0.145
Tamaño 750: sppedup: 0.148
Tamaño 1000: speedup: 0.148


5. *Explica los resultados del algoritmo guided, en función del reparto de carga de
trabajo que realiza*

Según los resultados (y siguiendo la teoría) el algoritmo guided debería ser tan 
rápido como el algoritmo, solo que tiene un gasto adicional en el que debe de ir 
actualizando el número de iteraciones restantes. Por eso es más lento que dynamic.



## EJERCICIO 3

1. ¿Cómo se comportará? Compáralo con el ejercicio anterior.

En este caso, el último bucle no llega hasta 'dim' sino hasta i, por lo tanto, cada vez que avanza la 'i' en el for más
grande (o primero) cada vez irá siendo menor el trabajado a realizar.


2. ¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior.

Por la explicación dad en la pregunta anterior, lo mejor sería un 'guided', ya que hace lo mismo, dar grandes cargas de trabajo
al inicio, y menores al final.


3. ¿Cuál es el peor algoritmo para este caso?

El peor algoritmo para este caso sería el 'static' porque le daría a todos los threads la misma cantidad de trabajo cuando
la función no funciona así.