#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>

#define RAND rand() % 100

void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim);
void matmul_sup_static (float *A, float *B, float *C, int dim);
void matmul_sup_dynamic (float *A, float *B, float *C, int dim);
void matmul_sup_guided (float *A, float *B, float *C, int dim);
void matmul_inf (float *A, float *B, float *C, int dim);
void print_matrix (float *M, int dim);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int iteraciones;
	int i = 0;
	double SPEEDUP0 = 0;
	double SPEEDUP1 = 0;
	double SPEEDUP2 = 0;
	double SPEEDUP3 = 0;
	if (argc == 4) {
		iteraciones = atoi (argv[3]);
	}else{
		i = 1;
	}

	int block_size = 1, dim;
	float *A, *B, *C;
	FILE *f = fopen("data.dat", "a");
  	if (f == NULL)
  	{
    	printf("Error opening file!\n");
    	exit(1);
  	}

   dim = atoi (argv[1]);
   if (argc == 3) block_size = atoi (argv[2]); 


	for(i; i< iteraciones; i++){
	   	A = (float *) malloc (dim * dim * sizeof (float));
	   	B = (float *) malloc (dim * dim * sizeof (float));
	   	C = (float *) malloc (dim * dim * sizeof (float));
		
	   	double tiempoIniPar, tiempoFinPar, tiempoSecuancial;
	   	double tiempoIni, tiempoFin;

	   	init_mat_sup(dim, A);
	   	//printf("Matriz A:\n");
	   	//print_matrix(A, dim);
	   	init_mat_inf(dim, B);   
	   	//printf("Matriz B:\n");
	   	//print_matrix(B, dim);

	   	// SPEEDUP = PARALELO(static, dynamic...)/SECUENCIAL

	   	int num_threads = omp_get_max_threads();
		/*printf("Numero de threads: %d\n", num_threads);*/

	   	tiempoIni = omp_get_wtime();
	   	/*Ejercicio 1 */
		matmul (A, B, C, dim);
		tiempoFin = omp_get_wtime();
		tiempoSecuancial=tiempoFin-tiempoIni;
		printf("Tiempo secuencial (iteracion [%d]): %f\n", i,tiempoFin-tiempoIni); 

	  	tiempoIniPar = omp_get_wtime();
	   	/*Ejercicio 1 */
		matmul (A, B, C, dim);
		tiempoFinPar = omp_get_wtime();
		printf("Tiempo paralelo (iteracion [%d]): %f\n", i,tiempoFinPar-tiempoIniPar); 
		printf("SPEEDUP (paralelo): %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
		if(i == 0){
			SPEEDUP0 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial);
		} else {
			SPEEDUP0 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial+SPEEDUP0)/2;
		}
		//print_matrix(C, dim);

		/* Ejercicio 2 */
		tiempoIniPar = omp_get_wtime();
		matmul_sup_static (A, B, C, dim);
		tiempoFinPar = omp_get_wtime();
		printf("Tiempo static (iteracion [%d]): %f\n", i,tiempoFinPar-tiempoIniPar);
		printf("SPEEDUP static: %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
		if(i == 0){
			SPEEDUP1 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial);
		} else {
			SPEEDUP1 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial+SPEEDUP1)/2;
		}

		//print_matrix(C, dim);
		tiempoIniPar = omp_get_wtime();
		matmul_sup_dynamic (A, B, C, dim);
		tiempoFinPar = omp_get_wtime();
		printf("Tiempo dynamic (iteracion [%d]): %f\n", i,tiempoFinPar-tiempoIniPar);
		printf("SPEEDUP dynamic: %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
		if(i == 0){
			SPEEDUP2 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial);
		} else {
			SPEEDUP2 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial+SPEEDUP1)/2;
		}
		
		//print_matrix(C, dim);
		tiempoIniPar = omp_get_wtime();
		matmul_sup_guided (A, B, C, dim);
		tiempoFinPar = omp_get_wtime();
		printf("Tiempo guided (iteracion [%d]): %f\n\n", i,tiempoFinPar-tiempoIniPar); 
		printf("SPEEDUP guided: %f\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
		if(i == 0){
			SPEEDUP3 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial);
		} else {
			SPEEDUP3 = ((tiempoFinPar-tiempoIniPar)/tiempoSecuancial+SPEEDUP3)/2;
		}
		//print_matrix(C, dim);

	} // fin for 
	fprintf(f,"0 paralelo %.3f\n",SPEEDUP0);
	fprintf(f,"1 static %.3f\n",SPEEDUP1);
	fprintf(f,"2 dynamic %.3f\n",SPEEDUP2);
	fprintf(f,"3 guided %.3f\n",SPEEDUP3);
	  	fclose(f);
		exit (0);

}

void print_matrix (float* Matriz, int dimension){
	int i, j;
	for (i=0; i < dimension; i++)
	{
		for (j=0; j < dimension; j++){
			printf (" %0.f ", Matriz[i*dimension+j]);			
		}
		printf("\n");
	}
	printf("\n\n");
}


void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];	
} 

void matmulParallel (float *A, float *B, float *C, int dim)
{
	int i, j, k;	
	#pragma omp parallel 
	{
		#pragma omp for private(i,j)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;
		#pragma omp for private(i,j,k)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				for (k=0; k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}	
} 

void matmul_sup_static (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	
	#pragma omp parallel 
	{
		#pragma omp for schedule(static) private(i,j)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(static) private (i,j,k)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
	
} 

void matmul_sup_dynamic (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel 
	{
		#pragma omp for schedule(dynamic) private(i,j)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(dynamic) private(i,j,k)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 

void matmul_sup_guided (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel 
	{
		#pragma omp for schedule(guided) private(i,j)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(guided) private(i,j,k)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
} 