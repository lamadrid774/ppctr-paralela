#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>

#define RAND rand() % 100

void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim);
void matmul_sup_static (float *A, float *B, float *C, int dim);
void matmul_sup_dynamic (float *A, float *B, float *C, int dim);
void matmul_sup_guided (float *A, float *B, float *C, int dim);
void matmul_inf (float *A, float *B, float *C, int dim);
void print_matrix (float *M, int dim);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;


   dim = atoi (argv[1]);
   if (argc == 3) block_size = atoi (argv[2]); 

   // TODO

   A = (float *) malloc (dim * dim * sizeof (float));
   B = (float *) malloc (dim * dim * sizeof (float));
   C = (float *) malloc (dim * dim * sizeof (float));
	
   double tiempoIniPar, tiempoFinPar;
   double tiempoIni, tiempoFin;

   init_mat_sup(dim, A);
   //printf("Matriz A:\n");
   //print_matrix(A, dim);
   init_mat_inf(dim, B);   
   //printf("Matriz B:\n");
   //print_matrix(B, dim);

   // SPEEDUP = PARALELO(static, dynamic...)/SECUENCIAL

   int num_threads = omp_get_max_threads();
	printf("Numero de threads: %d\n", num_threads);

	printf("SECUENCIAL\n");
   tiempoIni = omp_get_wtime();
   /*Ejercicio 1 */
	matmul (A, B, C, dim);
	tiempoFin = omp_get_wtime();
	printf("Tiempo: %f\n", tiempoFin-tiempoIni); 

	printf("PARALELO\n");
   tiempoIniPar = omp_get_wtime();
   /*Ejercicio 1 */
	matmul (A, B, C, dim);
	tiempoFinPar = omp_get_wtime();
	printf("Tiempo: %f\n", tiempoFinPar-tiempoIniPar); 
	printf("SPEEDUP: %f\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
	//print_matrix(C, dim);

	/* Ejercicio 2 */
	printf("Static:\n");
	tiempoIniPar = omp_get_wtime();
	matmul_sup_static (A, B, C, dim);
	tiempoFinPar = omp_get_wtime();
	printf("Tiempo: %f\n", tiempoFinPar-tiempoIniPar);
	printf("SPEEDUP: %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni)); 

	//print_matrix(C, dim);
	printf("Dynamic:\n");
	tiempoIniPar = omp_get_wtime();
	matmul_sup_dynamic (A, B, C, dim);
	tiempoFinPar = omp_get_wtime();
	printf("Tiempo: %f\n", tiempoFinPar-tiempoIniPar); 
	printf("SPEEDUP: %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
	
	//print_matrix(C, dim);
	printf("Guided:\n");
	tiempoIniPar = omp_get_wtime();
	matmul_sup_guided (A, B, C, dim);
	tiempoFinPar = omp_get_wtime();
	printf("Tiempo: %f\n", tiempoFinPar-tiempoIniPar); 
	printf("SPEEDUP: %f\n\n", (tiempoFinPar-tiempoIniPar)/(tiempoFin-tiempoIni));
	//print_matrix(C, dim);

	exit (0);
}

void print_matrix (float* Matriz, int dimension){
	int i, j;
	for (i=0; i < dimension; i++)
	{
		for (j=0; j < dimension; j++){
			printf (" %0.f ", Matriz[i*dimension+j]);			
		}
		printf("\n");
	}
	printf("\n\n");
}


void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];	
} 

void matmulParallel (float *A, float *B, float *C, int dim)
{
	int i, j, k;	
	#pragma omp parallel private(i,j,k)
	{
		#pragma omp for 
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;
		#pragma omp for 
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				for (k=0; k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}	
} 

void matmul_sup_static (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	
	#pragma omp parallel private(i,j,k)
	{
		#pragma omp for schedule(static)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp for schedule(static)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
	
} 

void matmul_sup_dynamic (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel num_threads (omp_get_max_threads()) private(i,j,k)
	{
		#pragma omp schedule(dynamic)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp schedule(dynamic)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 

void matmul_sup_guided (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel num_threads (omp_get_max_threads()) private(i,j,k)
	{
		#pragma omp schedule(guided)
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				C[i*dim+j] = 0.0;

		#pragma omp schedule(guided)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
} 

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
} 