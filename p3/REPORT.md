# P3: VideoTask

## Índice

1. Explicación del código
2. Comentario sobre los resultados
3. Mejoras del código (*Errores*)
4. Compilación y ejecución del código
5. EJERCICIOS

# 1. Explicación del código

## Explicación de la paralelización del código
La parte que paralelizamos en el archivo video_taskParalelo es toda la parte del 'do while'.
Para ello creamos una región paralela pero solo irá trabajando un solo thread que hará lo siguiente:
	- en un primer bucle cuenta y lee las imágenes que le van llegando, solo trabaja un único thread
	- a la hora de procesar las imágenes, sigue trabajando un único thread, pero este lo que hace es
	crear tareas, con lo que, de esta manera el resto de threads que no están trabajando se encargan de 
	ejecutar esas tareas que va dejando el thread principal. No continuamos con el programa hasta que
	no se hayan ejecutado todas las tareas pendientes.
	- por último el thread principal escribe las imágenes.
En líneas generales, utilizamos un thread que vaya creando tareas para que el resto las hagan. (Eso es 
el paralelismo principal del programa).

## Explicación del programa principal
Lo que hace el programa principal al igual que en la práctica anterior es ser ejecutado varias veces
según el número de iteraciones que le pasemos como argumento y va haciendo la media de los tiempos 
y calcula el speedup al final donde lo escribe en un archivo llamado 'f'.

## Explicación del archivo 'benchsuite.sh' 
Nuestro archivo benchsuite.sh básicamente ejecuta los dos programas (el secuencial y el paralelo) un 
número de veces (iteraciones) que nosotros le pasemos por parámetro a la hora de ejecutarlo. A continuación,
calcula el speedup dividiendo los tiempos obtenidos y escritos en el archivo 'data.dat'. De esta manera 
ya tiene los 3 resultados que va a pintar en la gráfica para obtener conclusiones de los mismos.
Por último, antes de pintar la gráfica, compara las dos películas de salida para que nos notifique
que ambas son idénticas o que difieren en caso de no ser iguales. 

# 2. Comentario sobre los resultados

## Observación de resultados 
Haciendo el benchmark y observando los resultados que nos das, ya sean ambos tiempos y el speedup; podemos
ver que, nos da un speedup realmente bueno, ya que no sobrepasa por lo general el 0.2. Gracias a la
instrucción 'diff' comprobamos que los programas ejecutan lo mismo, solo que nuestro programa paralelo
lo hace a una velocidad mucho mayor.

# 3. Mejoras del código (*Errores*)

## Errores y correcciones producidas en el código
Al inicio de la realización de la práctica, intentaba paralelizar el primer bucle donde
reservamos espacio en memoria, pero tras hacer varias pruebas, comprobando solo el tiempo
que tardaba en realizarse ese for, me di cuenta de que tardaba más si lo hacía de una manera
paralela (con #pragma omp for) que si lo dejaba como la manera secuencial. Por lo tanto, para
obtener un mejor tiempo lo he dejado de manera secuencial.
Lo mismo me ha pasado con la operación fgauss, la cual he intentado paralelizar con #pragma omp for
pero me da un speedup mayor que si no lo modifico (explicado en una de las cuestiones del ejercicio), 
por lo tanto, lo he dejado como estaba.

# 4. Compilación y ejecución del código

## ***Compilacion y ejecucion***

Para compilar y ejecutar el código debemos de hacer lo siguiente:
	- colocarnos en 'p3'
	- ejecutar './benchsuite'
	- los parámetros que le pasamos en este caso es el numero de veces que queremos que se ejecute 
	nuestros programas

**Ejemplo:**
```sh
user@x:/ppctr-paralela/p4$ ./benchsuite.sh 100
```

# 5. EJERCICIOS

## EJERCICIO 1
1. *Explica que funcionalidad has tenido que añadir/modificar sobre el código original. Explica 
si has encontrado algún fallo en la funcionalidad (“lo que hace el
programa”, independientemente al paralelismo) y cómo lo has corregido. Atención:
encontrar el fallo es algo “para nota”, por lo que no te deberías preocupar si no lo
encuentras.*

Lo que he cambiado del código es toda la parte del 'do while' donde he hecho 3 bucles
que cada uno de ellos hagan las distintas funciones que se deben hacer. En el primer bucle vamos
contando las imágenes que leemos para ver cuántas veces tenemos que hacer las siguiente funciones,
para esto usamos un contador de imágenes. Después hacemos dos bucles, uno que genere tareas que 
deban de procesar las imágenes y el otro que las escriba.


2. *Explica brevemente la función que desarrolla cada una de las directivas y funciones
de OpenMP que has utilizado, salvo las que ya hayas explicado en la práctica
anterior (por ejemplo parallel).*

A parte de parallel y for (usadas y explicadas en la práctica anterior) uso 3 directivas nuevas:
	- single: esta directiva lo que hace es ejecutar todo el bloque solamente un thread.
	- task: con esta directiva creamos una tarea la cual metemos en la lista de tares que 
	han de realizar los threads.
	- taskwait: también usamos taskwait, esto hace que el programa espere a que todas
	las tareas hayan terminado de realizarse. 


3. *Etiqueta todas las variables y explica por qué le has asignado esa etiqueta.*

Se ponen las directivas 'i' y 'contadorImagenes' privadas porque las usamos dentro de la región paralela y no
necesitamos que estén modificadas después, las demás se suponen como 'shared', ya que necesitamos modificarlas 
dentro de la región paralela y que sigan modificadas después.

4. *Explica cómo se consigue el paralelismo en este programa (aplicable sobre la función main).*

Creamos una región paralela en la que cuando llegue un hilo este creará varios thread. A continuación,
hacemos un single para que trabaje solo uno de los thread, de esta manera este thread, al procesar las
imágenes dejará una lista de tareas las cuales las irán haciendo el resto de threads. Gracias a taskwait
el porgrama espera a que los threads hayan terminado de hacer todas las tareas creadas.


5. *Calcula la ganancia (speedup) obtenido con la paralelización y explica los resultados obtenidos.*

Tras realizar la gráfica con la media de tiempos de varias ejecuciones vemos con nos da un speedup
bastante bueno, que por lo general no llega al 0,2. Gracias a la instrucción 'diff' comprobamos que
las dos películas de salida son iguales, por lo tanto, nuestro programa paralelo hace lo mismo que 
el programa secuencial, pero de una manera mucho más rápida.


6. *¿Se te ocurre alguna optimización sobre la función fgauss? Explica qué has hecho
y qué ganancia supone con respecto a la versión paralela anterior.*

Una forma que se me ha ocurrido para poder optimizar la función gauss es crear una zona paralela 
y dentro programar el bucle de forma dinámica para que lo vayan ejecutando los thread y que vayan
obteniendo carga de trabajo cada vez que terminan, pero ya solo la creación de varios hilos hace que 
mi programa tare más y tenga un peor speedup que la versión anterior.
