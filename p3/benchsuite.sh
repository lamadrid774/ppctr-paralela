#!/usr/bin/env bash
if ! [ -x "$(command -v gnuplot)" ]; then #Si no exite gnuplot...
  echo >&2 "Instalando GNUPLOT para dibujar las graficas"
  sudo apt-get install gnuplot-x11 #Instalamos gnuplot
  echo >&2 "Ejecuta de nuevo el script para realizar el benchsuite"
  exit #Salimos del programa
fi
file=resultado.log #Nombre del archivo .log
data=data.dat #Nombre del arhivo .dat
nexec=$1 #Numero de iteraciones
rm resultado.log 2> /dev/null #Comentar si no quereis que se borre el fichero resultado cada vez que ejecuteis el script
touch $file #Creamos el archivo .log
lscpu >> $file #Introducimos los datos del ordenador
lshw >> $file
make build > /dev/null #Compilamos la solución
./build/generator  # results dumped 1200 +-= 10s Cambiar si tienes un pepino ordenador o una calculadora rgb
./build/video_taskParalelo 8 $nexec 
./build/video_task 8 $nexec
diff -s movie.out movieParalelo.out 
array=$(awk '{ print $3 }' data.dat)
set -- $array 
speedup=$(echo "scale=4;$1/$2" | bc -l)
echo "3 speedUp $speedup" >> $data 
gnuplot 2> /dev/null <<EOF
set term png size 1000,1000
set output "graficaPractica3.png"
set style line 1 lc rgb 'red'
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'yellow'
set title "Grafica P3 (videoTask)"
unset key
set style fill solid
set yrange [0:6]
set xrange [0:4]
set boxwidth 0.5 abs
set ylabel 'Tiempo(s)' offset 2,1 rotate by 0
plot 'data.dat' every ::0::0 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 1,'' using 1:3:3 with labels,'data.dat' every ::1::1 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 2,'' using 1:3:3 with labels,'data.dat' every ::2::2 using 1:3:xtic(2) with boxes fs solid 0.25 linestyle 3,'' using 1:3:3 with labels, 
EOF
rm data.dat > /dev/null #Borramos el archivo data.dat
rm movie.in
rm movie.out 
rm movieParalelo.out
echo -ne "\033[0K\r"
echo "GRAFICA CREADA"