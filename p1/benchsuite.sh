#!/usr/bin/env bash
if ! [ -x "$(command -v gnuplot)" ]; then
  echo >&2 "Instalando GNUPLOT para dibujar las graficas"
  sudo apt-get install gnuplot-x11
fi
file=resultado.log
nexec=$1
touch $file
lscpu >> $file
lshw >> $file
make build > /dev/null
for season in `seq 1 1 $(($nexec))`; do
  for benchcase in st1 mt1 st2 mt2; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      ./runner.sh $benchcase $i >> $file # results dumped
      sleep 2
    done
  done
done
echo -ne "\033[0K\r"
echo "Resultados guardados"