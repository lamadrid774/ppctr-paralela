#!/usr/bin/env bash
size=$((512000 * $2))
nthreads=$(nproc)
case "$1" in
    st1)
        ./build/p1 $size xor
        ;;
    mt1)
        ./build/p1 $size xor --multi-thread $nthreads
        ;;
    st2)
        ./build/p1 $size sum
        ;;
    mt2)
        ./build/p1 $size sum --multi-thread $nthreads
        ;;
esac
