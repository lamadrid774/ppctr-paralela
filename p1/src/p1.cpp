#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <pthread.h>
#define FEATURE_LOGGER
/* thread, mutex y condicion variable de C++ */

/* g++ -std=c++17 -pthread -o program program.cpp -lm */

std::mutex m;

/* operacion */
void operacion(int op, int tamano, int inicio,  double* array, double* resultado, int thread){
	double resul = 0;
	int i;
	if(op == 0){	 	
		for (i=inicio; i<tamano; i++) {
			resul = resul + array[i];
		}
	}else if(op == 1){		
		for (i = inicio; i < tamano ; i++) {
			resul = (short)resul ^ (short)array[i];
		}
	}
	{
		std::lock_guard<std::mutex> guard(m);
		*resultado = *resultado + resul;
		printf("[REGISTRO] El hilo %d ha finalizado\n", thread);
	}
}


/* main */
int main(int argc, const char *argv[]){

  int tamanoArray; /* tamano del array de numeros*/
  tamanoArray = atoi(argv[1]);
  const char* operacionSum = "sum"; /*operacion de sumar*/
  const char* operacionXor = "xor"; /*operacion xor*/
  double* array; /* array de numeros */
  array = (double*) malloc(tamanoArray*sizeof(double)); /* reservamos memoria para el array*/
  int numThreads; /* numero de threads que metemos*/
  double resultado; /* donde guardamos el resultado de las operaciones*/

  /* comprobamos si los parametros que le pasamos no son los adecuados */ 
	if(argc < 3 || argc == 4 || argc > 5){
		printf("Numero de argumentos erroneo\n");
	}else{
		/* creacion del array de threads (comandos 'bien pasados') */
		for(int i=0; i<tamanoArray; i++){
			array[i] = i;
		}
		/* probando array */
		/*printf("[");
		for(int i=0; i<tamanoArray; i++){
			printf(" %2.f,", array[i]);
		}
		printf("]\n");
		printf("tamaño del array: %d\n", tamanoArray);*/
	}


 /* CASO DEL SINGLETHREAD */
 	if(argc == 3){ /* si numero de parametros son 2 estamos en single thread */
	 std::thread threads[1];
	 	if(strcmp(operacionSum,argv[2])==0){
	 		printf("Realizando operacion suma\n");
	 		operacion(0, tamanoArray, 0, array, &resultado, 1);
	 		printf("Resultado: %2.f\n", resultado);
	 	}else if(strcmp(operacionXor,argv[2])==0){
	 		printf("Realizando operacion xor\n");
	 		operacion(1, tamanoArray, 0, array, &resultado, 1);
	 	}else{
	 		printf("Operando mal introducido\n");
	 	}
	}


  	/* CASO DEL MULTITHREAD */
  	if(argc == 5){
    	if(strcmp("--multi-thread", argv[3])==0){
   			numThreads = atoi(argv[4]);
  			std::thread threads[numThreads];
      		printf("numero de threads: %d\n", numThreads);
      		if(tamanoArray < numThreads){
				numThreads = tamanoArray;
			}
      		double division = tamanoArray/numThreads;
	    	if(strcmp(operacionSum,argv[2])==0){
				printf("Realizando operacion suma\n");
				for (int i = 0; i<numThreads; i++){
					if(tamanoArray%numThreads == 0){
						threads[i] = std::thread(operacion, 0,division*i + division , division*i, array, &resultado, i);
					}else{
						if(i==0){
							threads[i] = std::thread(operacion, 0, division + 1 , 0, array, &resultado, i);
						}else{
							threads[i] = std::thread(operacion, 0, division*i + division +1, division*i +1, array, &resultado, i);
						}
					}
				}
				for (auto i=0; i<numThreads; i++){
					threads[i].join();
				}
				printf("resultado de la suma: %2.f \n", resultado);
			}else if(strcmp(operacionXor,argv[2])==0){
				printf("Realizando operacion xor\n");
				for (int i = 0; i < numThreads; i++) {
					if(tamanoArray%numThreads == 0){
						threads[i] = std::thread(operacion, 1,division*i + division , division*i, array, &resultado, i);
					}else{
						if(i==0){
							threads[i] = std::thread(operacion, 1, division + 1 , 0, array, &resultado, i);
						}else{
							threads[i] = std::thread(operacion, 1, division*i + division +1, division*i +1, array, &resultado, i);
						}
					}			  	}
				for (auto i=0; i<numThreads; ++i){
					threads[i].join();
		    	}
				printf("resultado de la operacion xor: %2.f \n", resultado);
			}
		}	
  	}
}