# P1: Multithreading en C++

# Índice
1. Tiempo invertido
2. Realización y dificultades
3. Explicación del código.

## 1. **Indica el tiempo que has necesitado para realizar la práctica y el tiempo invertido
en cada una de las secciones de la rúbrica.** 

Para esta práctica he invertido alrededor de unas 10 horas, teniendo en cuenta las horas
de prácticas (donde he utilizado dos sesiones de laboratorio), horas empleadas con mis 
compañeros en la biblioteca comparando código y/o resultados (unas 2 horas) y el tiempo
empleado en casa (sobre las 4 horas).
Sobre todo ha sido en la base la mayoría del tiempo, para el registro he usado sobre una 
hora aprox. He dedicado también tiempo a una posible realización del logger (sobre la hora 
u hora y media).


## 2. **Indica qué has conseguido y qué te falta por cada apartado de la rúbrica. Además, 
describe las dificultades encontradas durante dicho proceso.**

La parte de la Base la tengo completa, con una funcionalidad correcta en cualquiera de los
4 distintos casos (multi-thread, single-thread, suma o xor). Luego la parte del logger no 
la he realizado, y la parte del registro, como dice el enunciado, al no tener el logger 
hago que el registro lo muestre el main.


## 3. **Explica el funcionamiento del programa con mayor detalle que lo indicado en el
enunciado, haciendo especial hincapié en el paralelismo y las sincronizaciones. [∼5-40 líneas]**

Lo primero de todo es tener en cuenta varias cosas, el array que creamos le vamos metiendo
el valor del propio índice, es decir, la casilla 0 tendrá el valor 0, la casilla 1 el valor 
1, etc. Si tenemos más threads que tamaño de nuestro array de números, usaremos solo un número de
threads igual al tamaño. A la hora de trabajar en single thread, simplemente llamo a operación dependiendo 
del tipo de operación que pasamos por parámetro, al llamar a esta función de operación (la
cual recibirá como parámetro la operación a realizar) ella misma se encargará de 
recorrer el array (también pasado por parámetro como puntero) de los números que hemos creado
y va haciendo el cálculo de la operación (sum|xor) de todos los valores. 
Cuando llegamos a la parte del multi-thread, lo primero que hacemos es mirar la operación 
que debemos realizar ya sea suma o xor. Una vez comprobada la operación, hacemos un recorrido
del número de threads y los vamos creando llamando a la función operación. Al crearlos
debemos darle una carga de trabajo (una cantidad de números a operar) a cada thread
dependiendo del número de threads y el tamaño de nuestro array, ya que si no toca al mismo
número de operaciones a cada thread el primero se queda con uno más. Para esto comprobamos
el resto de la división, si es 0 operamos dándole a cada hilo el mismo trabajo, sino
comprobamos si es el primer thread y le damos un número más para operar, y al resto lo mismo.
